# Turn a Raspberry Pi 4 into an alarm clock using internet radio and lights to wake you up
The scope of this small project is to be able to replace a good old Philips SmartSleep alarm clock with lights. In fact I have a very old model which is only able to use FM radio (or 4 preset sounds) to wake me up after the lights have turned on.

## Prerequisites (Debian packages)
* Python 3
* Pip3
* alsa-base
* pulseaudio

## Prerequisites (Python libraries)
* pyhton-vlc
* python3-pil
* python-smbus
* python-i2ctools
* python3-rpi.gpio

## Prerequisites (hardware)
* Small LCD screen: Joy-It - SBC-LCD16x2
* Small 5V white LEDs: tested with SMD LED 2512 150mA (Würth Elektronik) and SMD LED 1210 60mA (Würth Elektronik)
