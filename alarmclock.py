import datetime
import time
import threading
import RPi.GPIO as GPIO
import os
import shelve
import vlc
import socket
import lib.i2c_lib as i2c_lib
import lib.lcddriver as lcddriver
import lightcontrol

light_crescendo_started = False
clock_must_run = True
alarm_clock_time = None
radio_is_on = False
max_volume = 100
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
# pins for buttons
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #set
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #left
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #right
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #up
GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #down

# pins for lights
GPIO.setup(31, GPIO.OUT)  # first
GPIO.setup(32, GPIO.OUT)  # second
GPIO.setup(35, GPIO.OUT)  # third
GPIO.setup(36, GPIO.OUT)  # forth
GPIO.setup(37, GPIO.OUT)  # fifth
GPIO.setup(38, GPIO.OUT)  # sixt

if not os.path.exists('AlarmClock.shelve.db'):
  # set default alarm clock time in DB if non found
  data = shelve.open('AlarmClock.shelve')
  data['alarmTime_hours'] = 7
  data['alarmTime_minutes'] = 30
  data.sync()
  data.close()

instance = vlc.Instance('--input-repeat=-1', '--fullscreen')
player = instance.media_player_new()

# function to set alarm clock based on DB information
def show_alarm_set_time():
  # open DB to set alarm clock time
  data = shelve.open('AlarmClock.shelve')
  # print in CLI the alarm clock time found in the data base
  print("{0:0>2d}:{1:0>2d}".format(data['alarmTime_hours'], data['alarmTime_minutes']))
  # print alarm clock time set on screen
  lcd = lcddriver.lcd()
  lcd.lcd_display_string("Setting alarm", 1)
  lcd.lcd_display_string("{0:0>2d}:{1:0>2d}".format(data['alarmTime_hours'], data['alarmTime_minutes']), 2)
  data.close()

# function to play music
def play_music():
  # define Radio Swiss Pop streaming URL
  url = "http://stream.srg-ssr.ch/m/rsp/mp3_128"
  media = None
  volume = 0

  # try to connect to URL
  try:
    socket.create_connection((url, 80))
    media = instance.media_new(url)
  # define other file to play
  except OSError:
    url = "music/1_1_Charlie-Winston_Spiral_6.flac"
    media = instance.media_new(url)
  # set media to play
  player.set_media(media)
  # set start volume
  player.audio_set_volume(volume)
  # start playing
  player.play()

  # while volume isn't at it's max turn up the volume by 1 every second
  while volume < max_volume:
    volume += 1
    player.audio_set_volume(volume)
    time.sleep(1)

# function updating the clock on the screen
def clock_function():
  global alarm_clock_time, clock_must_run, radio_is_on, light_crescendo_started
  now = datetime.datetime.now()
  data = shelve.open('AlarmClock.shelve')
  # define time to wake up based on setted hour and minute
  alarm_clock_time = datetime.datetime.now().replace(hour=data['alarmTime_hours'], minute=data['alarmTime_minutes'], second=0)

  # evergoing on loop
  while True:
    # check if clock must run
    if clock_must_run:
      # get time
      now = datetime.datetime.now()

      # display date and time on screen
      lcd = lcddriver.lcd()
      lcd.lcd_display_string(now.strftime("%d.%m.%Y %H:%M"), 1)

      # when alarm is within 30 minutes launch light crescendo
      if abs((alarm_clock_time - now).total_seconds())/60 < 30 and light_crescendo_started == False:
        light_crescendo_started = False
        threading.Thread(target=lightcontrol.crescendo(17, 1800, 600))

      # if within 5 second of alarm time start playing music
      if (abs((alarm_clock_time - now).total_seconds()/60)) < 5 and radio_is_on == False:
        radio_is_on = True
        play_music()

      # Update clock every 5 seconds
      time.sleep(5)

# function to allow to change settings
def updateTime(pos, value):
  global alarm_clock_time
  data = shelve.open('AlarmClock.shelve')
  if pos == 0:
    hours = data['alarmTime_hours']
    hours = hours + value
    if (hours >= 0 and hours < 24):
      data['alarmTime_hours'] = hours
    elif (hours >= 24):
      data['alarmTime_hours'] = 0
    elif (hours < 0):
      data['alarmTime_hours'] = 23
  if pos == 1:
    minutes = data['alarmTime_minutes']
    minutes = minutes + value
    if (minutes >= 0 and minutes < 60):
      data['alarmTime_minutes'] = minutes
    elif (minutes >= 60):
      data['alarmTime_minutes'] = 0
    elif (minutes < 0):
      data['alarmTime_minutes'] = 59
  data.sync()
  data.close()

# listen to keyboard changes
def keyboard_listner():
  global clock_must_run, player, radio_is_on
  pos = 0
  while True:
    # if setting button clicked
    if GPIO.input(10) == GPIO.HIGH:
      # if radio is on, turn it off
      if radio_is_on:
        player.stop()
      else:
        # stop showing clock
        clock_must_run = not clock_must_run
        data = shelve.open('AlarmClock.shelve')
        # show in CLI
        print("{0:0>2d}:{1:0>2d}".format(data['alarmTime_hours'], data['alarmTime_minutes']))

        # show on screen
        lcd = lcddriver.lcd()
        lcd.lcd_display_string("Setting alarm",1)
        lcd.lcd_display_string("{0:0>2d}:{1:0>2d}".format(data['alarmTime_hours'], data['alarmTime_minutes']), 2)

        # close DB
        data.close()
    # up button clicked
    if GPIO.input(13) == GPIO.HIGH:
      # increment of one to current position
      updateTime(pos, 1)
      show_alarm_set_time()
    # down button clicked
    if GPIO.input(15) == GPIO.HIGH:
      # decrement of one to current position
      updateTime(pos, -1)
      show_alarm_set_time()
    # left button clicked
    if GPIO.input(11) == GPIO.HIGH:
      # go back to hour setting
      show_alarm_set_time()
      if pos == 1:
        pos = 0
    # right button clicked
    if GPIO.input(12) == GPIO.HIGH:
      # go to minute setting
      show_alarm_set_time()
      if pos == 0:
        pos = 1


clock_thread = threading.Thread(target=clock_function)
clock_thread.start()

keyboard_listner_thread = threading.Thread(target=keyboard_listner)
keyboard_listner_thread.start()
