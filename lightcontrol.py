import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

# pins for lights, set all to output
GPIO.setup(31, GPIO.OUT)  # medium strength light bulb (250lm)
GPIO.setup(32, GPIO.OUT)  # medium strength light bulb (250lm)
GPIO.setup(35, GPIO.OUT)  # low strength light bulb (80lm)
GPIO.setup(36, GPIO.OUT)  # medium strength light bulb (250lm)
GPIO.setup(37, GPIO.OUT)  # medium strength light bulb (250lm)
GPIO.setup(38, GPIO.OUT)  # low strength light bulb (80lm)


def low_strength_light_crescendo(steps, time_in_secondes):
  if steps:
    light_1_80lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  if steps:
    light_2_80lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  if steps:
    light_1_80lm_on(False)
    time.sleep(0.01)
    light_2_80lm_on(False)
    time.sleep(0.01)
    pass

  return steps


def crescendo(steps, max_time_seconds, time_till_shutdown):
  time_in_secondes = max_time_seconds / steps

  # first block of 3 steps
  if steps:
    steps = low_strength_light_crescendo(steps, time_in_secondes)
    pass
  if steps:
    light_3_250lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  # second block of 3 steps
  if steps:
    steps = low_strength_light_crescendo(steps, time_in_secondes)
    pass
  if steps:
    light_4_250lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  # third block of 3 steps
  if steps:
    steps = low_strength_light_crescendo(steps, time_in_secondes)
    pass
  if steps:
    light_5_250lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  # forth block of 3 steps
  if steps:
    steps = low_strength_light_crescendo(steps, time_in_secondes)
    pass
  if steps:
    light_5_250lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  # fifth block of 3 steps
  if steps:
    steps = low_strength_light_crescendo(steps, time_in_secondes)
    pass
  if steps:
    light_6_250lm_on(True)
    time.sleep(time_in_secondes)
    steps -= 1
    pass

  # little extra to have complete light
  if steps:
    steps = low_strength_light_crescendo(steps, time_in_secondes)
    pass

  # could imagine go longer depending on number of lights need to wake you up ;-)

  # turn off all light if nothing done (forgot to turn off alarm clock for example)
  time.sleep(time_till_shutdown)
  light_1_80lm_on(False)
  light_2_80lm_on(False)
  light_3_250lm_on(False)
  light_4_250lm_on(False)
  light_5_250lm_on(False)
  light_6_250lm_on(False)

def light_1_80lm_on(status):
  GPIO.output(35, status)

def light_2_80lm_on(status):
  GPIO.output(38, status)

def light_3_250lm_on(status):
  GPIO.output(31, status)

def light_4_250lm_on(status):
  GPIO.output(32, status)

def light_5_250lm_on(status):
  GPIO.output(36, status)

def light_6_250lm_on(status):
  GPIO.output(37, status)
